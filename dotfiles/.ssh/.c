StrictHostKeyChecking no 
User dtimbadia
IdentityFile ~/.ssh/toms.key.for.timbadia.on.xps 
Include /cygdrive/c/project/TOMS/server.env/home/tomcat/.ssh/config 
Include /cygdrive/c/toms.devarea/caaspp/project/TOMS/server.env/home/tomcat/.ssh/config
# -------------------------------------------------------------
# ELPAC
# -------------------------------------------------------------
Host *
StrictHostKeyChecking no
ServerAliveInterval 60
IdentityFile /cygdrive/c/project/TOMS/server.env/home/tomcat/.ssh/elpac
KexAlgorithms diffie-hellman-group1-sha1

# -----------------------------------------------------------
# ELPAC DEV
# -----------------------------------------------------------
Host     e.dev.ui1 elpac.dev.ui1
User     tomcat
HostName etsvcln1477.etslan.org

Host     e.dev.ui2 elpac.dev.ui2
User     tomcat
HostName etsvcln1478.etslan.org

# Frozen # Host     e.dev.ui3 elpac.dev.ui3
# Frozen # User     tomcat
# Frozen # HostName etsvcln1479.etslan.org

# Frozen # Host     e.dev.ui4 elpac.dev.ui4
# Frozen # User     tomcat
# Frozen # HostName etsvcln1480.etslan.org

# Frozen # Host     e.dev.ui5 elpac.dev.ui5
# Frozen # User     tomcat
# Frozen # HostName etsvcln1481.etslan.org

# Frozen # Host     e.dev.ui6 elpac.dev.ui6
# Frozen # User     tomcat
# Frozen # HostName etsvcln1482.etslan.org

Host     e.dev.bat1 elpac.dev.bat1
User     tomcat
HostName etsvcln1483.etslan.org

Host     e.dev.bat2 elpac.dev.bat2
User     tomcat
HostName etsvcln1484.etslan.org

# Frozen # Host     e.dev.bat3 elpac.dev.bat3
# Frozen # User     tomcat
# Frozen # HostName etsvcln1485.etslan.org

# Frozen # Host     e.dev.bat4 elpac.dev.bat4
# Frozen # User     tomcat
# Frozen # HostName etsvcln1486.etslan.org

# Frozen # Host     e.dev.bat5 elpac.dev.bat5
# Frozen # User     tomcat
# Frozen # HostName etsvcln1487.etslan.org

Host     e.dev.api1 elpac.dev.api1
User     tomcat
HostName etsvcln1488.etslan.org

# -----------------------------------------------------------
# ELPAC ST
# -----------------------------------------------------------
Host     e.st.ui1 elpac.st.ui1
HostName etsvcln1489.etslan.org

Host     e.st.ui2 elpac.st.ui2
HostName etsvcln1490.etslan.org

# Frozen # Host     e.st.ui3 elpac.st.ui3
# Frozen # HostName etsvcln1491.etslan.org

Host     e.st.bat1 elpac.st.bat1
HostName etsvcln1492.etslan.org

Host     e.st.bat2 elpac.st.bat2
HostName etsvcln1493.etslan.org

# Frozen # Host     e.st.bat3 elpac.st.bat3
# Frozen # HostName etsvcln1494.etslan.org

# -----------------------------------------------------------
# ELPAC UAT
# -----------------------------------------------------------
Host     e.uat.ui1 elpac.uat.ui1
HostName etsvcln2005.etslan.org

Host     e.uat.ui2 elpac.uat.ui2
HostName etsvcln2006.etslan.org

# Frozen # Host     e.uat.ui3 elpac.uat.ui3
# Frozen # HostName etsvcln2007.etslan.org

Host     e.uat.bat1 elpac.uat.bat1
HostName etsvcln2008.etslan.org

Host     e.uat.bat2 elpac.uat.bat2
HostName etsvcln2009.etslan.org

# Frozen # Host     e.uat.bat3 elpac.uat.bat3
# Frozen # HostName etsvcln2010.etslan.org

# -----------------------------------------------------------
# ELPAC PROD
# -----------------------------------------------------------
Host     e.prod.ui1 elpac.prod.ui1
HostName etsvcln1542.etslan.org

Host     e.prod.ui2 elpac.prod.ui2
HostName etsvcln1543.etslan.org

Host     e.prod.ui3 elpac.prod.ui3
HostName etsvcln1544.etslan.org

# Frozen # Host     e.prod.ui4 elpac.prod.ui4
# Frozen # HostName etsvcln1545.etslan.org

# Frozen # Host     e.prod.ui5 elpac.prod.ui5
# Frozen # HostName etsvcln1546.etslan.org

# Frozen # Host     e.prod.ui6 elpac.prod.ui6
# Frozen # HostName etsvcln1547.etslan.org

Host     e.prod.bat1 elpac.prod.bat1
HostName etsvcln1548.etslan.org

Host     e.prod.bat2 elpac.prod.bat2
HostName etsvcln1549.etslan.org

Host     e.prod.bat3 elpac.prod.bat3
HostName etsvcln1550.etslan.org

# Frozen # Host     e.prod.bat4 elpac.prod.bat4
# Frozen # HostName etsvcln1061.etslan.org

# Frozen # Host     e.prod.bat5 elpac.prod.bat5
# Frozen # HostName etsvcln1077.etslan.org

# -------------------------------------------------------------
# EOF
# -------------------------------------------------------------

