;;; XEmacs backwards compatibility file
(setq user-init-file
      (expand-file-name "init.el"
			(expand-file-name ".xemacs" "~")))
(setq custom-file
      (expand-file-name "custom.el"
			(expand-file-name ".xemacs" "~")))

(load-file user-init-file)
(load-file custom-file)

; (load-file "/cygdrive/c/project/software/cygwin/usr/share/emacs/site-lisp/csv-mode.el")
; (load-file "/cygdrive/c/project/software/cygwin/home/tomcat/.escreen.el")
; (escreen-install)
; (setq escreen-prefix-char "\C-z")
; (global-set-key escreen-prefix-char 'escreen-prefix)
(require 'server)
(and (>= emacs-major-version 23)
     (defun server-ensure-safe-dir (dir) "Noop" t))
(when (and (eq window-system 'w32) (file-exists-p (getenv "APPDATA")))
  (setq server-auth-dir (concat (getenv "APPDATA") "/.emacs.d/server"))
  (make-directory server-auth-dir)  )
;(server-start)

