(setq csv-separators '("," ";" "|" "^"))
(setq-default indent-tabs-mode nil)
(setq c-basic-offset 2)
(add-hook 'c-mode-common-hook '(lambda () (c-toggle-auto-state 1)))
(add-hook 'c-mode-common-hook '(lambda () (c-toggle-hungry-state 1)))
;; ---------------------------------------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(display-time-mode t)
 '(inhibit-startup-screen t)
 '(package-archives
   (quote
    (("marmalade" . "https://marmalade-repo.org/packages/")
     ("melpa" . "http://melpa.org/packages/")
     ("org" . "http://orgmode.org/elpa/")))))
;; ---------------------------------------------
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:foreground "green" :background "black" :size "18pt" :family "Courier New" :foundry "outline" :slant normal :weight normal :height 102 :width normal))))
 '(buffers-tab ((t (:foreground "red" :background "black"))) t)
 '(custom-comment-face ((((class grayscale color) (background light)) (:foreground "yellow" :background "black"))))
 '(custom-state-face ((((class color) (background light)) (:foreground "dark green"))))
 '(font-lock-builtin-face ((((class color) (background light)) (:foreground "magenta"))))
 '(font-lock-comment-face ((t (:foreground "red"))))
 '(font-lock-constant-face ((t (:foreground "aquamarine"))))
 '(font-lock-doc-string-face ((((class color) (background light)) (:foreground "yellow" :dim nil))))
 '(font-lock-function-name-face ((((class color) (background light)) (:foreground "yellow" :background "darkblue"))))
 '(font-lock-keyword-face ((((class color) (background light)) (:foreground "wheat"))))
 '(font-lock-preprocessor-face ((((class color) (background light)) (:foreground "wheat"))))
 '(font-lock-string-face ((((class color) (background light)) (:foreground "lightsalmon" :background "black"))))
 '(font-lock-type-face ((((class color) (background light)) (:foreground "cyan"))))
 '(font-lock-variable-name-face ((((class color) (background light)) (:foreground "gold"))))
 '(gui-button-face ((t (:foreground "yellow" :background "blue" :bold t :italic t :underline t :inverse-video t))) t)
 '(isearch-secondary ((t (:foreground "yellow" :background "blue"))) t)
 '(list-mode-item-selected ((t (:foreground "red" :background "gray68"))) t)
 '(mode-line-buffer-id ((t (:overline "red" :underline "red"))))
 '(modeline ((t (:foreground "yellow"))) t)
 '(modeline-mousable ((t (:foreground "salmon"))) t)
 '(paren-match ((t (:foreground "red" :background "yellow"))) t)
 '(pointer ((t (:foreground "red"))) t)
 '(secondary-selection ((t (:foreground "red" :background "black" :bold nil))))
 '(toolbar ((t (:foreground "orange" :background "black"))) t)
 '(vertical-divider ((t (:foreground "green" :background "black"))) t)
 '(widget-field-face ((((class grayscale color) (background light)) (:foreground "orange" :background "black" :bold t))) t)
 '(zmacs-region ((t (:foreground "yellow" :background "red"))) t))
;; ---------------------------------------------

(display-time)

;(global-set-key "b" 'electric-buffer-list)
(global-set-key "r"  'replace-string)
(global-set-key "g"  'goto-line)

(global-set-key [f1] 'goto-line)
(global-set-key [f2] 'bookmark-set)
(global-set-key [f3] 'bookmark-jump)
(global-set-key [f4] 'bookmark-bmenu-list)
(global-set-key [f5] 'revert-buffer )
(global-set-key [f6] 'start-kbd-macro)
(global-set-key [f7] 'end-kbd-macro)
(global-set-key [f8] 'call-last-kbd-macro)

(global-set-key [M-f1] 'goto-line)
(global-set-key [M-f2] 'bookmark-set)
(global-set-key [M-f3] 'bookmark-jump)
(global-set-key [M-f4] 'bookmark-bmenu-list)
(global-set-key [M-f5] 'revert-buffer )
(global-set-key [M-f6] 'start-kbd-macro)
(global-set-key [M-f7] 'end-kbd-macro)
(global-set-key [M-f8] 'call-last-kbd-macro)

(global-set-key [C-f1] 'goto-line)
(global-set-key [C-f2] 'bookmark-set)
(global-set-key [C-f3] 'bookmark-jump)
(global-set-key [C-f4] 'bookmark-bmenu-list)
(global-set-key [C-f5] 'revert-buffer )
(global-set-key [C-f6] 'start-kbd-macro)
(global-set-key [C-f7] 'end-kbd-macro)
(global-set-key [C-f8] 'call-last-kbd-macro)

(global-unset-key [insert])


;; not sure what this does ...
(setq c-echo-syntactic-information-p t)
(setq minibuffer-max-depth nil)

